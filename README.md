# README

## Update Specific Config Client

	curl -X POST http://localhost:8881/refresh

	curl -X POST http://localhost:8882/refresh

	curl -X POST http://localhost:8883/refresh

## Update All Config Clients

	curl http://localhost:8880/monitor -d path="*"

	
## Set up RabbitMQ broker

Follow the instructions here

	https://spring.io/guides/gs/messaging-rabbitmq/


You can also use Docker Compose to quickly launch a RabbitMQ server if you have docker running locally.
In the root of the project there is a docker-compose.yml.

With this file in the current directory you can run 

	docker-compose up 

to get RabbitMQ running in a container.
