package org.parcaune.academy.config.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class MyController {

	@Value("${message}")
	private String message;

	@Value("${configuration.projectName}")
	private String projectName;

	@RequestMapping("/message")
	String message() {
		return message;
	}

	@RequestMapping("/project-name")
	String projectName() {
		return this.projectName;
	}

}